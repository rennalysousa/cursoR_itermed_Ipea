### AULA 27/09/2017 ###

#Instalação pacote devtools
install.packages("devtools")
library(devtools)

#Para pacotes não disponível no CRAN
help ("install_github")
install_github ("lucasmation/microdadosBrasil")

#Instalar pacotes
install.packages("readr")
library(readr)

### Questão 01
dic<- readr::read_csv(file = "bancos/dicionario_pessoas.csv")
dic<- readr:: read_csv(file.choose())
#read_csv (quando separado por ,); read_csv2 (quando separado por ;)

###Questão 02
str(widths)
help("fwr_whidths")
widths<-readr::fwf_widths(widths = dic$tamanho2,
                          col_names = dic$cod2)

pnad09.df<- readr::read_fwf(file = "bancos/AMOSTRA_DF_PNAD2009p.txt",
                            col_positions = widths)

widths<- readr::read_fwf(file="bancos/AMOSTRA_DF_PNAD2009p.txt")

## Questão 03
pnad_microBR<- microdadosBrasil:: read_PNAD(ft = "pessoas",
                                            i = 2009,
                                            file = "bancos/AMOSTRA_DF_PNAD2009p.txt")

